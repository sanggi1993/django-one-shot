from django.urls import path
from todos.views import (
    todo_list_list,
    todo_list_detail,
    create_todo_list,
    edit_todo_list,
    delete_todo_list,
    todo_item_create,
    todo_item_update,
)

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todo_list, name="create_todo_list"),
    path("<int:id>/edit/", edit_todo_list, name="edit_todo_list"),
    path("<int:id>/delete/", delete_todo_list, name="delete_todo_list"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("<int:id>/edit_item/", todo_item_update, name="todo_item_update"),
]
